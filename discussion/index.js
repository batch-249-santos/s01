// alert("Hello Batcht 249!");

let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

// What array method can we use to add an item at the end of the array?

students.push("Jace");
console.log(students);

// What array method can we use to add an item at the start of the array?

students.unshift("Jack");
console.log(students);

// Remove last item in the array

students.pop();
console.log(students);

// Remove the first item in the array

students.shift();
console.log(students);


// Difference between splice() and slice method ()

/*

splice  - (cuts) changes the original array
		- mutator method
		- can also be used in removing and adding items in our array from a starting index

slice   - (copies) uses an array as a reference to create a new array	
		- non mutator method


Iterator Methods
	- loop over the items of an array

forEach()
map()
every()

*/


let arrNum = [15, 20, 25, 30, 11, 7];

/*
	Using forEach(), check if every item is divisible by 5.
	If they are divisible by 5, log in the console "<num> is divisible by 5"/
	If not, log false.

*/

arrNum.forEach(n => {
	if (n % 5 === 0) {
		console.log(`${n} is divisible by 5`);
	} else {
		console.log(false);
	}		
});


// Math Object - allows you to perform mathematical tasks on numbers

// Math Constants

console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //PI
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); //square root of 1/2
console.log(Math.LN2); //natural logarithm of 2
console.log(Math.LN10); //natural logarithm of 10
console.log(Math.LOG2E); //base 2 of logarithm of E
console.log(Math.LOG10E); //base 10 of logarithm of E


//Methods for rounding a number to an integer
console.log(Math.round(Math.PI)); //3 -- round off
console.log(Math.ceil(Math.PI)); //4 -- round up
console.log(Math.floor(Math.PI)); //3 -- round down
console.log(Math.trunc(Math.PI)); //3 -- only the integer part

//returns a square root of a number
console.log(Math.sqrt(3.14))

//returns lowest value in our list of arguments
console.log(Math.min(-1, -2, -4, -100, 0, 2, 3, 5, 6));

//returns highest value in our list of arguments
console.log(Math.max(-1, -2, -4, -100, 0, 2, 3, 5, 6));

// Exponent

const number = 5 ** 2
console.log(number);

console.log(Math.pow(5, 2));



// Activity Quiz

/*

1.	Using array literals
2. 	array[0]
3.	array[array.length-1]
4.	array.indexOf()
5.	array.forEach()
6.  array.map()
7.	array.every()
8.	array.some()
9. false
10. true


*/



// Activity Function Coding
// 1.

function addToEnd (arr, x) {
	if (typeof x === "string"){
		arr.push(x);
		return arr
	}
	else {
		return ("Error: Can only add strings to an array.");
	}
}

 // 2.

 function addToStart (arr, x) {
	if (typeof x === "string"){
		arr.unshift(x);
		return arr
	}
	else {
		return ("Error: Can only add strings to an array.");
	}
}


 // 3.

 function elementChecker (arr, x) {
	if (arr.length != 0){
		if (arr.indexOf(x) == -1 ) {
			return false
		} else {
			return true
		}
	} else {
		return ("Error: Passed in array is empty.");
	}
}

 // 4.


function checkAllStringsEnding (arr, x) {

 	if (arr.length === 0){
 		return ("Error: Array must NOT be empty.");
 	} else if (arr.every(i => typeof i === "string") == false){
 		return ("Error: All array elements must be strings.")
 	} else if (typeof x != "string"){
 		return ("Error: Second argument must be of data type string.")
 	} else if (x.length != 1){
 		return ("Error: Second argument must be a single character.")
 	} else if (arr.every(i => i.toLowerCase().slice(-1) == x.toLowerCase())){
 		return true
 	} else {
 		return false
 	}
 }


 // 5.


 function stringLengthSorter (arr) {
 	if (arr.every(i => typeof i === "string") == false){
 		return ("Error: All array elements must be strings.")
 	} else {
 		const asc = arr.sort((a,b) => a.length - b.length);
 		return asc;
 	}
 }


 // 6.

 function startsWithCounter (arr, x) {

 	if (arr.length === 0){
 		return ("Error: Array must NOT be empty.");
 	} else if (arr.every(i => typeof i === "string") == false){
 		return ("Error: All array elements must be strings.")
 	} else if (typeof x != "string"){
 		return ("Error: Second argument must be of data type string.")
 	} else if (x.length != 1){
 		return ("Error: Second argument must be a single character.")
 	} else {
 		let count = 0
 		for (i=0; i < arr.length; i++) {
 			if (arr[i].toLowerCase().startsWith(x.toLowerCase())){
 				count++
 			}
 		}
 		return count
 	}
 }


 // 7.


 function likeFinder (arr, x) {

 	if (arr.length === 0){
 		return ("Error: Array must NOT be empty.");
 	} else if (arr.every(i => typeof i === "string") == false){
 		return ("Error: All array elements must be strings.")
 	} else if (typeof x != "string"){
 		return ("Error: Second argument must be of data type string.")
 	} else {

 		let newArr = []

 		for (i=0; i < arr.length; i++) {
 			if (arr[i].toLowerCase().includes(x.toLowerCase())){
 				newArr.push(arr[i])
 			}
 		}

 		return newArr
 	}
 }


 // 8.


 function randomPicker (arr) {

 	return arr[Math.floor(Math.random() * arr.length)]

 }